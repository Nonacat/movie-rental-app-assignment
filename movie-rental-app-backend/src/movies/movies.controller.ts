import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from 'src/auth/authenticated.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { Roles } from 'src/users/roles.decorator';
import { Role } from 'src/users/roles.enum';
import { Movie } from './movie.model';
import { MoviesService } from './movies.service';

@Controller('movies')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @Get()
  async getMovies() {
    const movies = await this.moviesService.getAllMovies();
    return movies;
  }

  @Get(':id')
  async getMovie(@Param('id') id: string) {
    const movie = await this.moviesService.getOneMovie(id);
    return movie;
  }

  @Post()
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async addMovie(@Body() movie: Movie) {
    const generatedId = await this.moviesService.addMovie(movie);
    return { id: generatedId };
  }

  @Patch(':id')
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async updateMovie(@Param('id') id: string, @Body() movie: Movie) {
    await this.moviesService.updateMovie(id, movie);
    return null;
  }

  @Delete(':id')
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async deleteMovie(@Param('id') id: string) {
    await this.moviesService.deleteMovie(id);
    return null;
  }
}
