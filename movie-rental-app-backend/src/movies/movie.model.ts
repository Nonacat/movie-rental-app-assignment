import * as mongoose from 'mongoose';

export const MovieSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    genre: { type: [String], required: true },
    director: { type: String, required: true },
    time_length: { type: String, required: false },
    rating: { type: Number, required: false },
    description: { type: String, required: false },
    actors: { type: [String], required: false },
    addition_date: { type: Date, required: true },
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true } },
);
MovieSchema.virtual('rental', {
  ref: 'Rental',
  localField: '_id',
  foreignField: 'movie',
  justOne: false,
});

export interface Movie extends mongoose.Document {
  id: string;
  title: string;
  genre: string[];
  director: string;
  time_length: string;
  rating: number;
  description: string;
  actors: string[];
  addition_date: Date;
}
