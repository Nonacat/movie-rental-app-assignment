import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Rental } from 'src/rentals/rental.model';
import { Movie } from './movie.model';

@Injectable()
export class MoviesService {
  constructor(
    @InjectModel('Movie') private readonly movieModel: Model<Movie>,
  ) {}

  async getAllMovies() {
    const movies = await this.movieModel.find().populate(['rental']).exec();
    movies.forEach((movie: any) => {
      const rentals = [];
      movie.rental.forEach((rental: Rental) => {
        if (!rental.return_date) {
          rentals.push(rental);
        }
      });
      movie.rental = rentals;
    });
    return movies
      .filter((movie: any) => movie.rental.length === 0)
      .map((movie) => ({
        id: movie.id,
        title: movie.title,
        genre: movie.genre,
        director: movie.director,
        rating: movie.rating,
      }));
  }

  async getOneMovie(movieId: string) {
    const movie = await this.findMovie(movieId);
    return {
      id: movie.id,
      title: movie.title,
      genre: movie.genre,
      director: movie.director,
      time_length: movie.time_length,
      description: movie.description,
      rating: movie.rating,
    };
  }

  async addMovie(movie: Movie) {
    const { id, ...rest } = movie;
    const newMovie = new this.movieModel({
      ...rest,
      addition_date: new Date(),
    });
    if (!rest.rating) {
      newMovie.rating = Math.floor(Math.random() * 10) + 1;
    }
    await newMovie.save();
  }

  async updateMovie(movieId: string, movie: Movie) {
    const movieDocument = await this.findMovie(movieId);
    for (const key in movie) {
      if (movieDocument[key]) {
        movieDocument[key] = movie[key];
      }
    }
    const result = await movieDocument.save();
    return result.id as string;
  }

  async deleteMovie(movieId: string) {
    const movieDocument = await this.findMovie(movieId);
    await movieDocument.deleteOne();
  }

  private async findMovie(id: string): Promise<Movie> {
    let movie: Movie;
    try {
      movie = await this.movieModel.findById(id);
    } catch (error) {
      throw new NotFoundException();
    }

    if (!movie) {
      throw new NotFoundException();
    }
    return movie;
  }
}
