import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { AuthenticatedGuard } from 'src/auth/authenticated.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { Movie } from 'src/movies/movie.model';
import { Roles } from 'src/users/roles.decorator';
import { Role } from 'src/users/roles.enum';
import { User } from 'src/users/user.model';
import { Rental } from './rental.model';
import { RentalsService } from './rentals.service';

@Controller('rentals')
export class RentalsController {
  constructor(private readonly rentalsService: RentalsService) {}

  @Get('all')
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async getAllRentals(@Req() req: Request) {
    const rentals = await this.rentalsService.getAllRentals();
    return rentals;
  }

  @Get()
  @Roles(Role.User)
  @UseGuards(RolesGuard)
  async getUsersRentals(@Req() req: Request) {
    const user = req.user as User;
    const rentals = await this.rentalsService.getUsersRentals(user.id);
    return rentals;
  }

  @Get(':username')
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async getUsersPendingRentalsByUsername(@Param('username') username: string) {
    const rentals = await this.rentalsService.getUsersRentalsByUsername(
      username,
    );
    return rentals;
  }

  @Post()
  @Roles(Role.User)
  @UseGuards(RolesGuard)
  async addRental(@Req() req: Request, @Body() movie: Movie) {
    const user = req.user as User;
    const status = await this.rentalsService.addRental(user.id, movie.id);
    return { status };
  }

  @Delete()
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async deleteRental(@Body() rental: Rental) {
    await this.rentalsService.archiveRental(rental.id);
  }
}
