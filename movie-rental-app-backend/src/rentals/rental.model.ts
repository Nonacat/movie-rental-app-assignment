import * as mongoose from 'mongoose';

export const RentalSchema = new mongoose.Schema(
  {
    client: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    movie: { type: mongoose.Schema.Types.ObjectId, ref: 'Movie' },
    rental_date: { type: Date, required: true },
    deadline_date: { type: Date, required: true },
    return_date: { type: Date, required: false },
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true } },
);

export interface Rental extends mongoose.Document {
  id: string;
  client: string;
  movie: string;
  rental_date: Date;
  deadline_date: Date;
  return_date: Date | null;
  archived: boolean | undefined;
}
