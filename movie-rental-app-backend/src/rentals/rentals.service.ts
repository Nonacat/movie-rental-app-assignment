import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Rental } from './rental.model';

@Injectable()
export class RentalsService {
  constructor(
    @InjectModel('Rental') private readonly rentalModel: Model<Rental>,
  ) {}

  async getAllRentals() {
    const rentals = await this.rentalModel.find().populate(['movie']).exec();
    return rentals;
  }

  async getUsersRentals(userId: string) {
    const rentals = await this.rentalModel
      .find({ id: userId })
      .populate(['movie'])
      .exec();
    return rentals;
  }

  async getUsersRentalsByUsername(username: string) {
    const rentals = await this.rentalModel
      .find({ username: username, return_date: null })
      .populate(['movie'])
      .exec();
    return rentals;
  }

  async addRental(userId: string, movieId: string) {
    const movieRented = await this.rentalModel
      .find({ client: userId, movie: movieId, return_date: { $ne: null } })
      .count();
    if (movieRented > 0) {
      return false;
    }

    const allMoviesRented = await this.rentalModel
      .find({ client: userId, return_date: { $ne: null } })
      .count();
    if (allMoviesRented >= 3) {
      return false;
    }

    const newRental = new this.rentalModel({
      client: userId,
      movie: movieId,
      rental_date: new Date(),
      deadline_date: (() => {
        const date = new Date();
        date.setDate(date.getDate() + 2);
        return date;
      })(),
    });
    await newRental.save();
    return true;
  }

  async deleteRental(rentalId: string) {
    await this.rentalModel.findByIdAndDelete(rentalId);
  }

  async archiveRental(rentalId: string) {
    await this.rentalModel.findByIdAndUpdate(rentalId, {
      return_date: new Date(),
    });
  }
}
