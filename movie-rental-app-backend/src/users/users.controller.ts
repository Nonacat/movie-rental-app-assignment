import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthenticatedGuard } from 'src/auth/authenticated.guard';
import { LocalGuard } from 'src/auth/local.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { Roles } from './roles.decorator';
import { Role } from './roles.enum';
import { User } from './user.model';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('logout')
  @UseGuards(AuthenticatedGuard)
  async logout(@Req() req: Request, @Res() res: Response) {
    req.logout();
    req.session.destroy(null);

    res.sendStatus(HttpStatus.OK);
  }

  @Post('login')
  @UseGuards(LocalGuard)
  async login(@Req() req: Request) {
    return { success: true };
  }

  @Post('register')
  async register(@Body() user: User) {
    await this.usersService.addUser(user);
    return { username: user.username };
  }

  @Get()
  @Roles(Role.Admin)
  @UseGuards(RolesGuard)
  async getAllUsernames() {
    const usernames = await this.usersService.getAllUsernames();
    return usernames;
  }

  @Get('current-user')
  @UseGuards(AuthenticatedGuard)
  async currentUser(@Req() req: Request) {
    const user = req.user as User;
    const userDocument = await this.usersService.getUser(user.username);
    return {
      username: userDocument.username,
      first_name: userDocument.first_name,
      last_name: userDocument.last_name,
      role: userDocument.role,
    };
  }

  @Get('current-user-full')
  @UseGuards(AuthenticatedGuard)
  async currentUserFull(@Req() req: Request) {
    const user = req.user as User;
    const userDocument = await this.usersService.getUser(user.username);
    return {
      username: userDocument.username,
      email_address: userDocument.email_address,
      role: userDocument.role,

      first_name: userDocument.first_name,
      last_name: userDocument.last_name,
      address: userDocument.address,
      phone_number: userDocument.phone_number,
      registration_date: userDocument.registration_date,
    };
  }
}
