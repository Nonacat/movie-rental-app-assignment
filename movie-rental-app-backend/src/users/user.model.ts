import * as mongoose from 'mongoose';
import { Role } from './roles.enum';

export const UserSchema = new mongoose.Schema({
  username: String,
  email_address: String,
  password: String,
  role: String,

  first_name: String,
  last_name: String,
  address: {
    city: String,
    zip_code: String,
    street: String,
  },
  phone_number: String,
  registration_date: Date,
});

export interface User extends mongoose.Document {
  id: string;
  username: string;
  email_address: string;
  password: string;
  role: Role;

  first_name: string;
  last_name: string;
  address: {
    city: string;
    zip_code: string;
    street: string;
  };
  phone_number: string;
  registration_date: Date;
}
