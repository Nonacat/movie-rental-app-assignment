import {
  Injectable,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Role } from './roles.enum';
import { User } from './user.model';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getAllUsernames() {
    const usernames = await this.userModel
      .find({ role: { $ne: Role.Admin } }, { username: true })
      .exec();
    return usernames;
  }

  async getUser(username: string) {
    const userDocument = await this.findUser(username);
    return userDocument;
  }

  async addUser(user: User) {
    if (!this.requiredFieldsFilled(user)) {
      throw new NotAcceptableException();
    }
    if (await this.usernameExists(user.username)) {
      throw new NotAcceptableException();
    }

    const newUser = new this.userModel({
      ...user,
      role: Role.User,
    });
    await newUser.save();
    return { username: newUser.username };
  }

  private async findUser(username: string): Promise<User | undefined> {
    let user: User;
    try {
      user = await this.userModel.findOne({ username }).exec();
    } catch (error) {
      throw new NotFoundException();
    }

    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  private requiredFieldsFilled(user: User) {
    return (
      user &&
      user.username &&
      user.email_address &&
      user.password &&
      user.first_name &&
      user.last_name
    );
  }

  private async usernameExists(username: string) {
    let user: User | undefined;
    try {
      user = await this.findUser(username);
    } catch (error) {}

    return !!user as boolean;
  }
}
