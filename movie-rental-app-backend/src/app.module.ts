import { join } from 'path';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MoviesModule } from './movies/movies.module';
import { RentalsModule } from './rentals/rentals.module';
import { ServeStaticModule } from '@nestjs/serve-static';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'),
      exclude: ['/api*'],
    }),
    MongooseModule.forRoot(
      `${process.env.MONGODB_URI || 'mongodb://localhost'}/movie-rental`,
      {
        user: process.env.MONGODB_USER || '',
        pass: process.env.MONGODB_PASSWORD || '',
      },
    ),
    UsersModule,
    AuthModule,
    MoviesModule,
    RentalsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
