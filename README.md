# Movie rental app

<img src="https://gitlab.com/Nonacat/movie-rental-app-assignment/badges/main/pipeline.svg">

The app initially was created as an assignment for a college course *Nierelacyjne Bazy Danych*. A user can **C**reate, **R**ead, **U**pdate and **D**elete the movies (given they have the permissions to do so).

It's automatically built and pushed to Heroku, if the build is successful. The app can be accessed with the link below:<br />
<a target="_blank" href="https://movie-rental-app-assignment.herokuapp.com">https://movie-rental-app-assignment.herokuapp.com</a>

---

- [x] Documentation
- [x] Create
- [x] Read
- [x] Update/Delete
- [x] HTTPS
- [x] API authorization and roles
- [x] GIT repository
- [x] CI/CD
- [ ] ~~Automatic tests~~
- [x] Responsive application
- [ ] ~~Application as Docker container~~
- [ ] ~~Microservices architecture~~
- [ ] ~~Additional cloud services~~

## Technologies

### Frontend

It's a SPA app, it uses VueJS with Vuetify UI framework.

### Backend

The backend was created using NestJS framework.


## Usage

### Adding a movie

1. Log into admin account
1. Click *Panel administratora* in the footer
1. Click *Dodaj film* button
1. Fill in the details
1. Click *Zapisz* button

### Editing a movie

1. Log into admin account
1. Click *Panel administratora* in the footer
1. Click *Edytuj* button for a given movie in the list 
1. Edit the details
1. Click *Zapisz* button

### Removing a movie

1. Log into admin account
1. Click *Panel administratora* in the footer
1. Click *Usuń* for a given movie in the list 
