import { Movie } from './movie';

export class Rental {
  constructor(
    public client?: string,
    public movie?: Movie,
    public rental_date?: Date,
    public deadline_date?: Date,
    public return_date?: Date,
  ) {}
}
