import { Role } from '@/enums/role';

export class User {
  constructor(
    public username?: string,
    public password?: string,
    public email_address?: string,
    public role?: Role,

    public first_name?: string,
    public last_name?: string,
    public address?: {
      city?: string;
      zip_code?: string;
      street?: string;
    },
    public phone_number?: string,
    public registration_date?: Date,
  ) {
    if (!address) this.address = {};
  }
}
