export class Movie {
  constructor(
    public id?: string,
    public title?: string,
    public genre?: string[],
    public director?: string,
    public rating?: number,
    public description?: string,
    public time_length?: string,
  ) {}
}
