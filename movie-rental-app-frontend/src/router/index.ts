import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

import Home from '@/views/Home.vue';
import MyAccount from '@/views/MyAccount.vue';
import MovieDetails from '@/views/MovieDetails.vue';
import AdminPanel from '@/views/AdminPanel.vue';
import { ComponentName } from '@/enums/componentName';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: ComponentName.Home,
    component: Home,
  },
  {
    path: '/my-account',
    name: ComponentName.MyAccount,
    component: MyAccount,
  },
  {
    path: '/admin-panel',
    name: ComponentName.AdminPanel,
    component: AdminPanel,
  },
  {
    path: '/:id',
    name: ComponentName.MovieDetails,
    component: MovieDetails,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
