import { User } from '@/models/user';
import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: new User(),
    axiosInstance: axios.create({
      baseURL: process.env.VUE_APP_API_URL,
      withCredentials: true,
    }),
  },

  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
  },

  getters: {
    isLoggedIn(state) {
      return !!state.user.username;
    },

    getUser(state) {
      return state.user;
    },

    getAxiosInstance(state) {
      return state.axiosInstance;
    },
  },

  actions: {
    async updateUserStatus({ getters, commit }) {
      let userInfo;
      try {
        userInfo = await getters.getAxiosInstance.get('/users/current-user');
      } catch (error) {
        console.error(error);
      }

      if (userInfo?.data) {
        commit('SET_USER', userInfo?.data);
      } else {
        commit('SET_USER', new User());
      }
    },
  },
  modules: {},
});
