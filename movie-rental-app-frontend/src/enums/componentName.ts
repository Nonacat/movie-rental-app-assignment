export enum ComponentName {
  Home = 'Home',
  MyAccount = 'MyAccount',
  AdminPanel = 'AdminPanel',
  MovieDetails = 'MovieDetails',
}
